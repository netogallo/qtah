-- This file is part of Qtah.
--
-- Copyright 2015-2023 The Qtah Authors.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QTextEdit (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  np,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Config (qtVersion)
import Graphics.UI.Qtah.Generator.Flags (flagsT)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (fl_Alignment, qreal)
import Graphics.UI.Qtah.Generator.Interface.Gui.QColor (c_QColor)
import Graphics.UI.Qtah.Generator.Interface.Gui.QFont (c_QFont)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  listener,
  listenerBool,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractScrollArea (c_QAbstractScrollArea)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QMenu (c_QMenu)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QTextEdit"] $
  [ QtExportClassAndSignals c_QTextEdit signals
  , qtExport e_LineWrapMode
  , qtExport e_AutoFormattingFlag
  , qtExport fl_AutoFormatting
  ]

(c_QTextEdit, signals) =
  makeQtClassAndSignals signalGens $
  addReqIncludes [includeStd "QTextEdit"] $
  classSetEntityPrefix "" $
  makeClass (ident "QTextEdit") Nothing [c_QAbstractScrollArea] $
  collect
  [ just $ mkCtor "new" np
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , just $ mkCtor "newWithText" [objT c_QString]
  , just $ mkCtor "newWithTextAndParent" [objT c_QString, ptrT $ objT c_QWidget]
  , just $ mkProp "acceptRichText" boolT
  , just $ mkProp "alignment" $ flagsT fl_Alignment
  , just $ mkConstMethod "anchorAt" [objT c_QPoint] $ objT c_QString
  , just $ mkMethod "append" [objT c_QString] voidT
  , just $ mkProp "autoFormatting" $ flagsT fl_AutoFormatting
  , just $ mkConstMethod "canPaste" np boolT
  , just $ mkMethod "clear" np voidT
  , just $ mkMethod "copy" np voidT
  , just $ mkMethod' "createStandardContextMenu" "createStandardContextMenu" np $ ptrT $ objT c_QMenu
  , just $ mkMethod' "createStandardContextMenu" "createStandardContextMenuAt" [objT c_QPoint] $
    ptrT $ objT c_QMenu
    -- TODO currentCharFormat
  , just $ mkProp "currentFont" $ objT c_QFont
    -- TODO cursorForPosition
  , just $ mkConstMethod' "cursorRect" "cursorRect" np $ objT c_QRect
    -- TODO cursorRect(const QTextCursor&)
  , just $ mkProp "cursorWidth" intT
  , just $ mkMethod "cut" np voidT
    -- TODO document
  , just $ mkProp "documentTitle" $ objT c_QString
  , just $ mkMethod "ensureCursorVisible" np voidT
    -- TODO extraSelections
  , just $ mkMethod' "find" "find" [objT c_QString] boolT
    -- TODO find with FindFlags
  , just $ mkProp "fontFamily" $ objT c_QString
  , just $ mkProp "fontItalic" boolT
  , just $ mkProp "fontPointSize" qreal
  , just $ mkProp "fontUnderline" boolT
  , just $ mkProp "fontWeight" intT
  , just $ mkMethod "insertHtml" [objT c_QString] voidT
  , just $ mkMethod "insertPlainText" [objT c_QString] voidT
  , just $ mkProp "lineWrapColumnOrWidth" intT
  , just $ mkProp "lineWrapMode" $ enumT e_LineWrapMode
    -- TODO loadResource
    -- TODO mergeCurrentCharFormat
    -- TODO moveCursor
  , just $ mkProp "overwriteMode" boolT
  , just $ mkMethod "paste" np voidT
    -- TODO print
  , just $ mkBoolIsProp "readOnly"
  , just $ mkMethod "redo" np voidT
  , just $ mkMethod "scrollToAnchor" [objT c_QString] voidT
  , just $ mkMethod "selectAll" np voidT
  , just $ mkMethod "setHtml" [objT c_QString] voidT
  , just $ mkMethod "setPlainText" [objT c_QString] voidT
  , just $ mkMethod "setText" [objT c_QString] voidT
  , just $ mkProp "tabChangesFocus" boolT
  , test (qtVersion < [6]) $ mkProp "tabStopWidth" intT
  , just $ mkProp "textBackgroundColor" $ objT c_QColor
  , just $ mkProp "textColor" $ objT c_QColor
    -- TODO textCursor
    -- TODO textInteractionFlags
  , just $ mkConstMethod "toHtml" np $ objT c_QString
  , just $ mkConstMethod "toPlainText" np $ objT c_QString
  , just $ mkMethod "undo" np voidT
  , just $ mkBoolIsProp "undoRedoEnabled"
    -- TODO wordWrapMode
  , just $ mkMethod "zoomIn" np voidT
  , just $ mkMethod' "zoomIn" "zoomInPoints" [intT] voidT
  , just $ mkMethod "zoomOut" np voidT
  , just $ mkMethod' "zoomOut" "zoomOutPoints" [intT] voidT
  ]

signalGens :: [SignalGen]
signalGens =
  [ makeSignal "copyAvailable" listenerBool
    -- TODO currentCharFormatChanged
  , makeSignal "cursorPositionChanged" listener
  , makeSignal "redoAvailable" listenerBool
  , makeSignal "selectionChanged" listener
  , makeSignal "textChanged" listener
  , makeSignal "undoAvailable" listenerBool
  ]

e_LineWrapMode =
  makeQtEnum (ident1 "QTextEdit" "LineWrapMode") [includeStd "QTextEdit"]
  [ "NoWrap"
  , "WidgetWidth"
  , "FixedPixelWidth"
  , "FixedColumnWidth"
  ]

(e_AutoFormattingFlag, fl_AutoFormatting) =
  makeQtEnumAndFlags (ident1 "QTextEdit" "AutoFormattingFlag") "AutoFormatting" [includeStd "QTextEdit"]
  [ "AutoNone"
  , "AutoBulletList"
  , "AutoAll"
  ]
