-- This file is part of Qtah.
--
-- Copyright 2015-2023 The Qtah Authors.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QFont (
  aModule,
  c_QFont,
  ) where

import Foreign.Hoppy.Generator.Spec (
  addReqIncludes,
  classSetConversionToGc,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkCtor,
  mkConstMethod,
  mkMethod,
  mkProp,
  np,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Config (qtVersion)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AQtModule $
  makeQtModule ["Gui", "QFont"] $
  collect
  [ just $ qtExport c_QFont
  , test (qtVersion >= [6]) $ qtExport e_Weight
  ]

c_QFont =
  addReqIncludes [includeStd "QFont"] $
  classSetConversionToGc $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetEntityPrefix "" $
  makeClass (ident "QFont") Nothing [] $
  collect
  [ just $ mkCtor "new" np
  , just $ mkConstMethod "bold" np boolT
  , test (qtVersion >= [6]) $ mkProp "legacyWeight" intT
  , just $ mkMethod "setBold" [boolT] voidT
  , just $ mkMethod "setPixelSize" [intT] voidT
  , just $ mkMethod "setPointSize" [intT] voidT
  , test (qtVersion < [6]) $ mkProp "weight" intT
  , test (qtVersion >= [6]) $ mkProp "weight" $ enumT e_Weight
  ]

e_Weight =
  makeQtEnum (ident1 "QFont" "Weight") [includeStd "QFont"]
  [ "Thin"
  , "ExtraLight"
  , "Light"
  , "Normal"
  , "Medium"
  , "DemiBold"
  , "Bold"
  , "ExtraBold"
  , "Black"
  ]

-- TODO The rest of QFont.
