-- This file is part of Qtah.
--
-- Copyright 2015-2023 The Qtah Authors.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

{-# OPTIONS_GHC -W -fwarn-incomplete-patterns -fwarn-unused-do-bind #-}
{-# LANGUAGE CPP, RankNTypes #-}

import Control.Applicative ((<|>))
import Control.Monad (unless, when)
import Data.Char (isDigit)
import Data.List (intercalate, isInfixOf, isPrefixOf)
import Data.Maybe (fromMaybe)
import Distribution.InstalledPackageInfo (libraryDirs)
import Distribution.Package (
  pkgName, unPackageName,
#if MIN_VERSION_Cabal(2,0,0)
  mkPackageName,
#else
  PackageName (PackageName),
#endif
  )
import Distribution.PackageDescription (
  PackageDescription,
  package,
#if MIN_VERSION_Cabal(2,0,0)
  mkFlagName,
#else
  FlagName (FlagName),
#endif
  )
import Distribution.Simple (defaultMainWithHooks, simpleUserHooks)
import Distribution.Simple.BuildPaths (autogenComponentModulesDir)
import Distribution.Simple.LocalBuildInfo (
  LocalBuildInfo,
  absoluteInstallDirs,
  buildDir,
  installedPkgs,
  libdir,
  localPkgDescr,
  )
import Distribution.Simple.PackageIndex (lookupPackageName)
import Distribution.Simple.Setup (
  CleanFlags,
  ConfigFlags,
  CopyDest (CopyTo, NoCopyDest),
  cleanVerbosity,
  configConfigurationsFlags,
  configVerbosity,
  copyDest,
  copyVerbosity,
  flagToMaybe,
  fromFlagOrDefault,
  installDistPref,
  installVerbosity,
  )
import Distribution.Simple.UserHooks (
  UserHooks (
    cleanHook,
    copyHook,
    instHook,
    postConf
    ),
  )
#if MIN_VERSION_Cabal(2,0,0)
import Distribution.Simple.Utils (die')
#else
import Distribution.Simple.Utils (die)
#endif
import Distribution.Simple.Utils (
  createDirectoryIfMissingVerbose,
  info,
  installOrdinaryFile,
  notice,
  )
import Distribution.Types.ComponentName (ComponentName (CLibName))
#if MIN_VERSION_Cabal(3,2,0)
-- GHC 8.10.1+:
import Distribution.Types.Flag (lookupFlagAssignment)
#elif MIN_VERSION_Cabal(2,2,0)
import Distribution.Types.GenericPackageDescription (lookupFlagAssignment)
#endif
#if MIN_VERSION_Cabal(3,0,0)
import Distribution.Types.LibraryName (LibraryName (LMainLibName))
#endif
import Distribution.Types.LocalBuildInfo (componentNameCLBIs)
import Distribution.Verbosity (Verbosity, normal)
import Graphics.UI.Qtah.Generator.Config (qtVersion)
import Graphics.UI.Qtah.Generator.Main (generateHs)
import System.Directory (
  doesDirectoryExist,
  doesFileExist,
  getCurrentDirectory,
  getDirectoryContents,
  removeDirectoryRecursive,
  removeFile,
  )
import System.Environment (lookupEnv, setEnv)
import System.FilePath ((</>), joinPath, takeDirectory)

type DieFn = forall a. String -> IO a

#if !MIN_VERSION_Cabal(2,0,0)
mkPackageName = PackageName
mkFlagName = FlagName
#endif

packageName :: String
-- Careful, this line is modified by set-qt-version.sh.
packageName = "qtah"

cppPackageName :: String
-- Careful, this line is modified by set-qt-version.sh.
cppPackageName = "qtah-cpp"

main :: IO ()
main = defaultMainWithHooks qtahHooks

qtahHooks :: UserHooks
qtahHooks = simpleUserHooks
  { postConf = \args cf pd lbi -> do generateSources cf lbi
                                     postConf simpleUserHooks args cf pd lbi
  , copyHook = \pd lbi uh cf -> do let verbosity = fromFlagOrDefault normal $ copyVerbosity cf
                                       dest = fromFlagOrDefault NoCopyDest $ copyDest cf
                                   doInstall verbosity pd lbi dest
                                   copyHook simpleUserHooks pd lbi uh cf
  , instHook = \pd lbi uh if' -> do let verbosity = fromFlagOrDefault normal $ installVerbosity if'
                                        dest = maybe NoCopyDest CopyTo $
                                               flagToMaybe $ installDistPref if'
                                    doInstall verbosity pd lbi dest
                                    instHook simpleUserHooks pd lbi uh if'
  , cleanHook = \pd z uh cf -> do doClean cf
                                  cleanHook simpleUserHooks pd z uh cf
  }

-- | The name of the file we'll use to hold the enum evaluation cache.
enumEvalCacheFileName :: FilePath
enumEvalCacheFileName = "qtah-enum-eval-cache"

defaultLibComponentName :: ComponentName
defaultLibComponentName =
#if MIN_VERSION_Cabal(3,0,0)
  CLibName LMainLibName
#else
  CLibName
#endif

-- | Locates the autogen directory for the library component.
getAutogenDir :: Verbosity -> LocalBuildInfo -> IO FilePath
getAutogenDir verbosity localBuildInfo = do
  let libCLBIs = componentNameCLBIs localBuildInfo defaultLibComponentName
#if MIN_VERSION_Cabal(2,0,0)
      dieFn = die' verbosity
#else
      dieFn = die
#endif

  case libCLBIs of
    [libCLBI] -> return $ autogenComponentModulesDir localBuildInfo libCLBI
    _ ->
      dieFn $ concat
      [packageName, ": Expected one library ComponentLocalBuildInfo with name ",
       show packageName, ", found ", show $ length libCLBIs, "."]

lookupQtahCppLibDir :: LocalBuildInfo -> DieFn -> IO FilePath
lookupQtahCppLibDir localBuildInfo dieFn = do
  -- Look for an installed qtah-cpp package.
  qtahCppPkg <- case lookupPackageName (installedPkgs localBuildInfo) $
                     mkPackageName cppPackageName of
    [(_, [qtahCppPkg])] -> return qtahCppPkg
    results ->
      dieFn $ concat
      [packageName, ": Failed to find a unique ", cppPackageName, " installation.  Found ",
       show results, "."]

  -- Look up the libDir of the qtah-cpp we found.  The filter here is for NixOS,
  -- where libraryDirs includes the library directories of dependencies as well.
  case filter (\x -> cppPackageName `isInfixOf` x) $ libraryDirs qtahCppPkg of
    [libDir] -> return libDir
    libDirs -> dieFn $ concat
               [packageName, ": Expected a single library directory for ",
                cppPackageName, ", got ", show libDirs, "."]

generateSources :: ConfigFlags -> LocalBuildInfo -> IO ()
generateSources configFlags localBuildInfo = do
  let verbosity = fromFlagOrDefault normal $ configVerbosity configFlags
#if MIN_VERSION_Cabal(2,0,0)
      dieFn = die' verbosity
#else
      dieFn = die
#endif

  -- Parse the Qt version to use from flags and the environment, and export it
  -- to the generator.
  qtVersion <- exportQtVersion configFlags localBuildInfo

  -- Ensure that we're using the same version of Qt that qtah-cpp is.
  qtahCppLibDir <- lookupQtahCppLibDir localBuildInfo dieFn
  let qtahCppQtVersionFile = qtahCppLibDir </> "qtah-qt-version"
  qtahCppQtVersion <-
    (\contents -> case lines contents of
       [line] -> return line
       _ -> dieFn $ concat
            [packageName, ": Expected a single line in ", qtahCppQtVersionFile, ", got ",
             show contents, "."]) =<<
    readFile qtahCppQtVersionFile
  when (qtVersion /= qtahCppQtVersion) $
    dieFn $ concat
    [packageName, ": Qt version mismatch between ", packageName, " (", qtVersion, ") and ",
     cppPackageName, " (", qtahCppQtVersion, ").  Please reconfigure one or the other."]

  -- Generate binding source code.
  let enumEvalCacheFilePath = qtahCppLibDir </> enumEvalCacheFileName
  autogenDir <- getAutogenDir verbosity localBuildInfo
  createDirectoryIfMissingVerbose verbosity True autogenDir
  generateHs autogenDir enumEvalCacheFilePath

doInstall :: Verbosity -> PackageDescription -> LocalBuildInfo -> CopyDest -> IO ()
doInstall verbosity packageDesc localBuildInfo copyDest = do
  -- Record what version of Qt we are using.
  let libDir = libdir $ absoluteInstallDirs packageDesc localBuildInfo copyDest
  createDirectoryIfMissingVerbose verbosity True libDir
  installOrdinaryFile verbosity
                      (buildDir localBuildInfo </> "qtah-qt-version")
                      (libDir </> "qtah-qt-version")

doClean :: CleanFlags -> IO ()
doClean cleanFlags = do
  startDir <- getCurrentDirectory

  -- Remove generated Haskell sources.
  delDir $ startDir </> joinPath ["src", "Graphics", "UI", "Qtah", "Generated"]
  delStartingWithInDir "Q" $ startDir </> joinPath ["src", "Graphics", "UI", "Qtah", "Core"]
  delStartingWithInDir "Q" $ startDir </> joinPath ["src", "Graphics", "UI", "Qtah", "Gui"]
  delStartingWithInDir "Q" $ startDir </> joinPath ["src", "Graphics", "UI", "Qtah", "Widgets"]
  delStartingWithInDir "Connection.hs" $ startDir </> joinPath ["src", "Graphics", "UI", "Qtah", "Core"]
  delStartingWithInDir "Types.hs" $ startDir </> joinPath ["src", "Graphics", "UI", "Qtah", "Core"]
  mapM_ (delFile True (startDir </> joinPath ["src", "Graphics", "UI", "Qtah", "Internal"]))
    ["Callback.hs", "EventListener.hs", "Listener.hs", "SceneEventListener.hs"]

  where verbosity = fromFlagOrDefault normal $ cleanVerbosity cleanFlags

        delFile checkExists dir file = do
          let path = dir </> file
          exists <- if checkExists
                    then doesFileExist path
                    else return True
          when exists $ do
            info verbosity $ concat ["Removing file ", path, "."]
            removeFile path

        delDir path = do
          exists <- doesDirectoryExist path
          when exists $ do
            info verbosity $ concat ["Removing directory ", path, "."]
            removeDirectoryRecursive path

        delIt dir file = do
          let path = dir </> file
          fileExists <- doesFileExist path
          if fileExists
            then delFile True dir file
            else do dirExists <- doesDirectoryExist path
                    when dirExists $ delDir path

        delStartingWithInDir prefix dir = do
          exists <- doesDirectoryExist dir
          when exists $
            mapM_ (delIt dir) .
            filter (prefix `isPrefixOf`) =<<
            getDirectoryContents dir

-- | This function should be called in a 'postConf' hook.  It determines the
-- requested Qt version based on package flags and the program environment, and
-- sets the environment variables @QTAH_QT@ and @QT_SELECT@ appropriately.
--
-- The mutually exclusive package flags @qtX@ specify a preference on
-- a major version of Qt.  Additionally, the environment variable @QTAH_QT@ can
-- either be @x@ or @x.y@ to specify a major or minor version of Qt,
-- respectively.  If both QTAH_QT and a flag is specified, they must agree on
-- the major version of Qt.  If using QTAH_QT, it only needs to be set for the
-- configure phase.  If neither flags nor QTAH_QT are set, then the system
-- default Qt version (as determined by @qmake@) will be used.  This may be
-- influenced by @qtchooser@.
--
-- If this package's name ends with @-qtX@, then Qt X (major version only) is
-- used unconditionally.  This overrides the above methods.
--
-- This returns the preferred major version of Qt, if there is a preference
-- (@Maybe Int@), along with the Qt version string returned from qtah-generator
-- (@String@).
--
-- !!! KEEP THIS FUNCTION IN SYNC WITH qtah-cpp/Setup.hs !!!
exportQtVersion :: ConfigFlags -> LocalBuildInfo -> IO String
exportQtVersion configFlags localBuildInfo = do
  let verbosity = fromFlagOrDefault normal $ configVerbosity configFlags
#if MIN_VERSION_Cabal(2,0,0)
      dieFn = die' verbosity
#else
      dieFn = die
#endif

  -- Determine what version of Qt to use.  If we have a Qt version preference
  -- specified, either through package flags or through QTAH_QT, then
  -- maybeQtMajor will get that value.
  let myName = pkgName $ package $ localPkgDescr localBuildInfo
  maybeQtMajor <- case reverse $ unPackageName myName of
    -- If the package name ends in "-qtX", then build for Qt X (whatever the
    -- available minor version is).  Ignore QTAH_QT and package flags.
    n:'t':'q':'-':_ | isDigit n -> do
      setEnv "QTAH_QT" [n]
      notice verbosity $ concat [packageName, ": Requesting Qt ", [n], " because of package name."]
      return $ Just (read [n] :: Int)

    -- Otherwise, we'll inspect the environment and flags.
    _ -> do
      -- Inspect the 'qtX' package flags.
      let flags = configConfigurationsFlags configFlags
#if MIN_VERSION_Cabal(2,2,0)
          lookupFlag = lookupFlagAssignment
#else
          lookupFlag = lookup
#endif
          qt4Flag = fromMaybe False $ lookupFlag (mkFlagName "qt4") flags
          qt5Flag = fromMaybe False $ lookupFlag (mkFlagName "qt5") flags
          qt6Flag = fromMaybe False $ lookupFlag (mkFlagName "qt6") flags
          qtFlag | qt4Flag = Just 4
                 | qt5Flag = Just 5
                 | qt6Flag = Just 6
                 | otherwise = Nothing
          toInt b = if b then 1 else 0
      when (toInt qt4Flag + toInt qt5Flag + toInt qt6Flag > 1) $
        dieFn $
        packageName ++ ": The qtX flags are mutually exclusive.  Please select at most one."

      -- Inspect the QTAH_QT environment variable.
      qtahQtStr <- lookupEnv "QTAH_QT"
      qtahQtMajor <- case qtahQtStr of
        Just s | not $ null s -> do
          let majorStr = takeWhile (/= '.') s
          unless (all isDigit majorStr) $
            dieFn $ concat [packageName, ": Invalid QTAH_QT value ", show s,
                          ".  Expected a numeric version string."]
          return $ Just (read majorStr :: Int)
        _ -> return Nothing

      -- Determine which version of Qt to use, and put it in QTAH_QT for the
      -- generator to pick up.
      case (qtahQtMajor, qtFlag) of
        -- If both QTAH_QT and one of the qtX flags above is set, then they must agree.
        (Just m, Just n) | m /= n ->
          dieFn $ concat
          [packageName, ": QTAH_QT=", show $ fromMaybe "" qtahQtStr, " and the qt",
           show n, " flag conflict."]
        -- Otherwise, if QTAH_QT is not already set but we have a flag preference,
        -- then use QTAH_QT to tell qtah-generator about the flag.
        (Nothing, Just n) -> setEnv "QTAH_QT" $ show n
        _ -> return ()

      -- Log a message showing which Qt we're requesting.
      case (qtahQtMajor, qtFlag) of
        (Just m, _) ->
          notice verbosity $
          concat [packageName, ": Requesting Qt ", show m, " because of QTAH_QT=",
                  show $ fromMaybe "" qtahQtStr, "."]
        (_, Just n) ->
          notice verbosity $
          concat [packageName, ": Requesting Qt ", show n, " because of the qt", show n, " flag."]
        _ -> notice verbosity $ packageName ++ ": Requesting system default Qt."

      return $ qtahQtMajor <|> qtFlag

  -- If we have a major version preference, then set QT_SELECT in case we're
  -- calling QMake.  We use QT_SELECT over "-qt=X" because it doesn't break when
  -- qtchooser isn't available.
  case maybeQtMajor of
    Just qtMajor -> setEnv "QT_SELECT" $ show qtMajor
    Nothing -> return ()

  -- Log a message showing which Qt qtah-generator is actually using.
  let qtVersionStr = intercalate "." $ map show qtVersion
  notice verbosity $ concat [packageName, ": Using Qt ", qtVersionStr, "."]

  -- Record the selected Qt version in a file for later installation.
  let qtVersionFile = buildDir localBuildInfo </> "qtah-qt-version"
  createDirectoryIfMissingVerbose verbosity True $ takeDirectory qtVersionFile
  writeFile qtVersionFile $ qtVersionStr ++ "\n"

  return qtVersionStr
