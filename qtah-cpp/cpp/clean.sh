#!/usr/bin/env bash

# This file is part of Qtah.
#
# Copyright 2023 The Qtah Authors.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Cleans up generated build outputs.

set -euo pipefail
if [[ $(uname) = Darwin ]] && which greadlink >/dev/null 2>&1; then
    readlink() { greadlink "$@"; }
fi
myDir=$(readlink -f "$0")
myDir=$(dirname "$myDir")
declare -r myDir

cd "$myDir"

rm -f ./Makefile
rm -f ./b_*
rm -f ./moc_*
rm -f ./listener.{cpp,hpp}
rm -f ./*.o
rm -f ./libqtah.*dylib
rm -f ./libqtah.so*
